# OpenML dataset: Air-Traffic-Data

https://www.openml.org/d/43512

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

San Francisco International Airport Report on Monthly Passenger Traffic Statistics by Airline. Airport data is seasonal in nature, therefore any comparative analyses should be done on a period-over-period basis (i.e. January 2010 vs. January 2009) as opposed to period-to-period (i.e. January 2010 vs. February 2010). It is also important to note that fact and attribute field relationships are not always 1-to-1. For example, Passenger Counts belonging to United Airlines will appear in multiple attribute fields and are additive, which provides flexibility for the user to derive categorical Passenger Counts as desired.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43512) of an [OpenML dataset](https://www.openml.org/d/43512). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43512/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43512/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43512/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

